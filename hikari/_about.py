# -*- coding: utf-8 -*-
# cython: language_level=3
# Copyright © Nekoka.tt 2019-2020
#
# This file is part of Hikari.
#
# Hikari is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Hikari is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with Hikari. If not, see <https://www.gnu.org/licenses/>.
"""Package metadata."""

from __future__ import annotations

# DO NOT ADD TYPE HINTS TO THESE FIELDS. THESE ARE AUTOMATICALLY UPDATED
# FROM THE CI SCRIPT AND DOING THIS MAY LEAD TO THE DEPLOY PROCESS FAILING.

__author__ = "Nekokatt"
__ci__ = "https://gitlab.com/nekokatt/hikari/pipelines"
__copyright__ = "© 2019-2020 Nekokatt"
__discord_invite__ = "https://discord.gg/Jx4cNGG"
__docs__ = "https://nekokatt.gitlab.io/hikari"
__email__ = "3903853-nekokatt@users.noreply.gitlab.com"
__issue_tracker__ = "https://gitlab.com/nekokatt/hikari/issues"
__is_official_distributed_release__ = False
__license__ = "LGPL-3.0-ONLY"
__url__ = "https://gitlab.com/nekokatt/hikari"
__version__ = "2.0.0.dev0"
