# -*- coding: utf-8 -*-
# Copyright © Nekoka.tt 2019-2020
#
# This file is part of Hikari.
#
# Hikari is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Hikari is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with Hikari. If not, see <https://www.gnu.org/licenses/>.
from __future__ import annotations

import asyncio

import aiohttp
import mock


class RequestContextStub:
    def __init__(self, response_getter) -> None:
        self.response_getter = response_getter
        self.await_count = 0

    async def __aenter__(self) -> None:
        return self.response_getter()

    async def __aexit__(self, exc_type, exc_val, exc_tb) -> None:
        pass

    def __await__(self) -> aiohttp.ClientResponse:
        # noinspection PyUnreachableCode
        if False:
            yield  # Turns this into a generator.
        self.await_count += 1
        return self.response_getter()

    def assert_awaited_once(self):
        assert self.await_count == 1


class ClientSessionStub:
    def __init__(self) -> None:
        self.close = mock.AsyncMock()
        self.closed = mock.PropertyMock()
        self.connector = mock.Mock(spec_set=aiohttp.BaseConnector)
        self.cookie_jar = mock.Mock(spec_set=aiohttp.CookieJar)
        self.version = aiohttp.HttpVersion11

        self.response_stub = mock.Mock(spec_set=aiohttp.ClientResponse)
        self.websocket_stub = mock.Mock(spec_set=aiohttp.ClientWebSocketResponse)

        self.request_context_stub = RequestContextStub(lambda: self.response_stub)
        self.ws_connect_stub = RequestContextStub(lambda: self.websocket_stub)

        self.request = mock.Mock(wraps=lambda *args, **kwargs: self.request_context_stub)
        self.ws_connect = mock.Mock(wraps=lambda *args, **kwargs: self.ws_connect_stub)

        for method in "get put patch post delete head options".split():
            self._make_method(method)

    def _make_method(self, method) -> None:
        shim = mock.Mock(wraps=lambda *a, **k: self.request(method, *a, **k))
        setattr(self, method, shim)

    @property
    def loop(self):
        return asyncio.current_task().get_loop()

    async def __aenter__(self) -> ClientSessionStub:
        return self

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        pass
